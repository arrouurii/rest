import { HttpClient } from '@angular/common/http';
import {Http} from '@angular/http';
import { Component, OnInit } from '@angular/core';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.css']
})
export class ContactsComponent implements OnInit {
  constructor(private http: Http) { }

  ngOnInit() {
    this.http.get('localhost:8090/contacts')
      .pipe(map(res => res.json()))
      .subscribe(
        elt => {
          console.log(elt);
        }
      );
  }

}
