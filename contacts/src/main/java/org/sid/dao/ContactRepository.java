package org.sid.dao;

import org.sid.entities.Contact;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by Nidhal on 17/08/2018.
 */
public interface ContactRepository extends JpaRepository<Contact,Long> {
    //@Query("select c from contact ct where ct.c.nom like :x")
    @Query(value = "select * from contact where nom LIKE :x", nativeQuery = true)
    public Page<Contact> chercher(@Param("x") String keyword, Pageable pageable);
}
