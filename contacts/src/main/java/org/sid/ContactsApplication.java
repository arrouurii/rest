package org.sid;

import javafx.scene.input.DataFormat;
import org.sid.dao.ContactRepository;
import org.sid.entities.Contact;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

@SpringBootApplication
public class ContactsApplication implements CommandLineRunner {
	@Autowired
	private ContactRepository contactRepository;
	public static void main(String[] args) {
		SpringApplication.run(ContactsApplication.class, args);
	}

	@Override
	public void run(String... strings) throws Exception {
		/*DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		contactRepository.save(new Contact(
				"nidhal",
				"arouri",
				df.parse("02/06/1994"),
				"a@gmail.com",
				9999999,
				"ddd"

		));

		contactRepository.save(new Contact(
				"Ali",
				"arouri",
				df.parse("02/06/1994"),
				"a@gmail.com",
				9999999,
				"ddd"

		));
		contactRepository.save(new Contact(
				"Ala",
				"arouri",
				df.parse("02/06/1994"),
				"a@gmail.com",
				9999999,
				"sss"

		));

		contactRepository.findAll().forEach( e-> {
			System.out.println(e.getNom());
		});*/
	}
}
