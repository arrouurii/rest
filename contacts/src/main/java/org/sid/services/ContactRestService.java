package org.sid.services;

import org.sid.dao.ContactRepository;
import org.sid.entities.Contact;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * Created by Nidhal on 17/08/2018.
 */
@RestController

public class ContactRestService {
    @Autowired
    private ContactRepository contactRepository;
    //@CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(value = "/contacts", method = RequestMethod.GET)
    public List<Contact> getContacts() {
        return contactRepository.findAll();
    }
    @RequestMapping(value = "/contacts/{id}", method = RequestMethod.GET)
    public Contact getContact(@PathVariable Long id) {
        Optional<Contact> contact = this.contactRepository.findById(id);
        if (contact.isPresent()) {
            return contact.get();
        }
        else
            return null;
    }
    @RequestMapping(value = "/contacts", method = RequestMethod.POST)
    public Contact save(@RequestBody Contact contact) {
        return contactRepository.save(contact);
    }
    @RequestMapping(value = "/contacts/{id}", method = RequestMethod.DELETE)
    public boolean delete(@PathVariable Long id) {
        contactRepository.deleteById(id);
        return true;
    }

    @RequestMapping(value = "/contacts/{id}", method = RequestMethod.PUT)
    public Contact save(@RequestBody Contact contact, @PathVariable Long id) {
        Contact currentContact = contactRepository.findById(id).get();
        if (contact.getDateNaissance() != null)
            currentContact.setDateNaissance(contact.getDateNaissance());
        if (contact.getEmail() != null)
            currentContact.setEmail(contact.getEmail());
        if (contact.getNom() != null)
            currentContact.setNom(contact.getNom());
        if (contact.getPrenom() != null)
            currentContact.setPrenom(contact.getPrenom());
        if (contact.getTel() != 0)
            currentContact.setTel(contact.getTel());
        if (contact.getPhoto() != null)
            currentContact.setPhoto(contact.getPhoto());
        return contactRepository.save(currentContact);
    }
    @RequestMapping(value = "/chercher-contacts", method = RequestMethod.GET)
    public Page<Contact> search(
                @RequestParam(name = "mc", defaultValue = "") String mc,
                @RequestParam(name = "page", defaultValue = "0") int page,
                @RequestParam(name = "size", defaultValue = "5") int size
            ) {
        return contactRepository.chercher("%"+mc+"%",new PageRequest(page,size));
    }
}
